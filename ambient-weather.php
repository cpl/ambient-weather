<?php
/**
 * Plugin Name:       Ambient Weather
 * Description:       Displays Current Temperature at CPL locations and links to additonal weather conditions
 * Requires at least: 6.4
 * Requires PHP:      8.1
 * Version:           0.2.3
 * Author:            The CPL Team (Tyrone Fontaine and Will Skora)
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ambient-weather
 * Domain Path:       CPL-Ambient-Weather
 * Update URI:        https://gitlab.com/cpl/ambient-weather
 *
 * @package           cpl
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
function ambient_weather_ambient_weather_block_init() {
	register_block_type( __DIR__ . '/build' );
}
add_action( 'init', 'ambient_weather_ambient_weather_block_init' );
