import { __ } from '@wordpress/i18n';

import { useBlockProps, InspectorControls } from '@wordpress/block-editor';

import { PanelBody, SelectControl } from '@wordpress/components';

import './editor.scss';

export default function Edit( { attributes, setAttributes } ) {
	// define the attributes
	const { branchSelection } = attributes;

	const onChangeBranchSelection = ( val ) => {
		setAttributes( { branchSelection: val } );
	};

	return (
		<>
			<InspectorControls>
				<PanelBody
					title={ __( 'Options', 'ambient-weather' ) }
					initialOpen={ true }
				>
					<SelectControl
						label={ __( 'Branch Location', 'ambient-weather' ) }
						onChange={ onChangeBranchSelection }
						value={ branchSelection }
						options={ [
							{
								label: __( 'Main', 'ambient-weather' ),
								value: 'main',
							},
							{
								label: __( 'Collinwood', 'ambient-weather' ),
								value: 'collinwood',
							},
							{
								label: __( 'Eastman', 'ambient-weather' ),
								value: 'eastman',
							},
							{
								label: __( 'Fulton', 'ambient-weather' ),
								value: 'fulton',
							},
							{
								label: __( 'Harvard-Lee', 'ambient-weather' ),
								value: 'harvard-lee',
							},
							{
								label: __( 'Lorain', 'ambient-weather' ),
								value: 'lorain',
							},
							{
								label: __( 'Woodland', 'ambient-weather' ),
								value: 'woodland',
							},
						] }
					/>
				</PanelBody>
			</InspectorControls>

			<p { ...useBlockProps() }>
				{ __(
					'Ambient Weather - Select the branch in the sidebar',
					'ambient-weather'
				) }
			</p>
		</>
	);
}
