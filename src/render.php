<?php
/**
 * @see https://github.com/WordPress/gutenberg/blob/trunk/docs/reference-guides/block-api/block-metadata.md#render
 */



	// in block.json, main is already set as the default value for $attributes['branchSelection']; do not need to
	// explicitly call it here

	$branch_selection = isset( $attributes['branchSelection'] ) ? $attributes['branchSelection'] : 'main';
	$weather_array    = array(
		'main'        => 'https://ambientweather.net/dashboard/0506ef10e507f534241a9afa4c817955',
		'collinwood'  => 'https://ambientweather.net/dashboard/0341f86e97ae90f6b5c80251556940c1',
		'eastman'     => 'https://ambientweather.net/dashboard/eb67ded03550b36e2b6dd0b07ad8707d',
		'fulton'      => 'https://ambientweather.net/dashboard/b888e4008eee64949d1679e8acf03ef8',
		'harvard-lee' => 'https://ambientweather.net/dashboard/6f5a6de74670d881e17859d73a49aa39',
		'lorain'      => 'https://ambientweather.net/dashboard/14d33a2ca8ce770571e96d6488f92025',
		'woodland'    => 'https://ambientweather.net/dashboard/3257d81172f89ae2202b02cd390e2367',
	);
	$the_url          = isset( $weather_array[ $branch_selection ] ) ? $weather_array[ $branch_selection ] : '';

	// whil
	$lib = $attributes['branchSelection'];

	$branch_name_short = $attributes['branchSelection'];

	// get the library branch from block.json that is being read;


	// get_transient returns false when transient has expired or doesn't exist
	// the if statement could probably be written simpler
	// https://developer.wordpress.org/apis/transients/#fetching-transients
	if ( false === ( $all = get_transient( 'transient_ambient_api' ) ) ) {
		// if ( $all ) {
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, 'https://rt.ambientweather.net/v1/devices?applicationKey=aee5a31d2dca41569022873b7c017395e3fdda29384a42b49d6404a8b9187f81&apiKey=4a56d5f742bc4517aac0d55b7c6e146f279da1cb7e3d4235af1c436f31124c83' );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HEADER, false );

		$response = curl_exec( $ch );

		// cache this response for 1 hour; you don't want to cache it if there's an error
		if ( $response === false ) {
			// Curl Error
			$error = curl_error( $ch );
			echo 'API Call Error: ' . $error;
		} else {
			// Curl request successful
			curl_close( $ch );


			$all = json_decode( $response, true );

			set_transient( 'transient_ambient_api', $all, 24 * 60 );
		}
	}



		/*
		*
		*   This is the string that matches in the loop.
		*
		*/
	if ( $lib == 'collinwood' ) {
		$lib = 'Cleveland Public Library - Collinwood Campus';
	} elseif ( $lib == 'eastman' ) {
		$lib = 'Cleveland Public Library - Eastman Campus';
	} elseif ( $lib == 'fulton' ) {
		$lib = 'Cleveland Public Library - Fulton Campus';
	} elseif ( $lib == 'harvard-lee' ) {
		$lib = 'Cleveland Public Library - Harvard-Lee Campus';
	} elseif ( $lib == 'lorain' ) {
		$lib = 'Cleveland Public Library - Lorain Campus';
	} elseif ( $lib == 'woodland' ) {
		$lib = 'Cleveland Public Library - Woodland Campus';
	} else {
		$lib = 'Cleveland Public Library - Main Campus';
	}
	?>
	<div class="ambient-weather">
		<div class="content">
			<span class="ambient-words">Learn More About The Weather At <?php echo esc_html( $attributes['branchSelection'] ); ?></span>
			<!-- <div class="ambient-mobile">	 -->
				<a target="_blank" class="ambient-weather-button ambient-link" href=<?php echo esc_url( $the_url ); ?>><img alt="Weather Dashboard for <?php echo esc_attr( $attributes['branchSelection'] ); ?> Branch" src="<?php echo esc_url( plugins_url( 'img/ambient-weather.svg', dirname( __FILE__ ) ) ); ?>">&nbsp;</a>
				<?php if ( $all ) : ?>
				<a class="ambient-weather-button ambient-temp" data-modal-open="weather_modal" >
					<span tabindex="-1" aria-hidden="true" class="ambient-degree" data-modal-open="weather_modal"></span>
					<img data-modal-open="weather_modal" alt="current weather at <?php echo esc_attr( $attributes['branchSelection'] ); ?> branch. Expand for additional weather details" tabindex="-1" aria-hidden="true" src="<?php echo esc_url( plugins_url( 'img/expand-arrows.svg', dirname( __FILE__ ) ) ); ?>"></a>
				<?php endif; ?>
			<!-- </div> -->
		</div>
	</div>

	<div id="weather_modal" data-modal>
		<div data-modal-document>

			<h2><?php echo( esc_html( ucwords( $branch_name_short ) ) ); ?> Weather</h2>

		<div>
		<?php

		if ( ! empty( $all ) && is_array( $all ) ) {

			foreach ( $all as $item ) {

				if ( isset( $item['info']['name'] ) && $item['info']['name'] === $lib ) {

					$w = $item['lastData'];

					// dateutc is in unix epoch time and includes 3 extra zeroes at the end; cut out those 0s;
					// create new datetime object, assign that object to the unixtime
					// format that time into a more presentable format with date_format
					$time_last_updated_unix = substr( $w['dateutc'], 0, -3 );
					$time_object            = new DateTime();
					$time_object->setTimestamp( $time_last_updated_unix );
					$formatted_updated_time = date_format( $time_object, 'l, F j Y g\:i a' );


					// round expects int and errors out otherwise, so don't think sanitize_text_field is needed in most places
					$temp      = round( $w['tempf'], 0 );
					$windspeed = round( $w['windspeedmph'], 1 );
					$winddir   = sanitize_text_field( $w['winddir'] );
					$rain      = round( $w['dailyrainin'], 1 );
					$barom     = round( $w['baromrelin'], 1 );

					if (!function_exists('degreesToCardinal')) {
						function degreesToCardinal( $degrees ) {
							$cardinals = array( 'N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N' );

							// Use intval to cast the result to an integer
							$index = intval( round( $degrees / 45 ) ) % 8;

							return $cardinals[ $index ];
						}
					}
					$cardinaldir = degreesToCardinal( $winddir );
					?>

				<ul class="weather-details">
					<li class="weather-temp">Temperature: <?php echo esc_html( $temp ); ?>&deg; F</li>
					<li class="weather-wind">Wind Speed: <?php echo esc_html( $windspeed ); ?>mph</li>
					<li class="weather-direction">Wind Direction: <?php echo esc_html( $cardinaldir ); ?></li>
					<li class="weather-rain">Daily Rain (Inches): <?php echo esc_html( $rain ); ?></li>
					<li class="weather-pressure">Barometric Pressure: <?php echo esc_html( $barom ); ?></li>
				</ul>

				<hr class="hr--alternative" />

				<p class="p--description_on_background_dark">Last updated:
					<?php
					echo ( esc_html( $formatted_updated_time ) );
					?>
				</p>

				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".ambient-degree").text("<?php echo esc_js( $temp ); ?>°");

						$('.ambient-weather-button span, .ambient-weather-button img').attr('tabindex', '-1');

						$('.ambient-weather-button span, .ambient-weather-button img').on('focus', function() {
							$(this).attr('tabindex', '-1');
						});
					});
				</script>

					<?php
				} // Close if
			} // Close foreach

		} else {
			echo 'Sorry, we could not connect to Ambient Weather API.';
		} // $all check


		?>

				<div class="phase-3" style="display: none">
					<hr>

					   <h3>Check out the <?php esc_html( the_title() ); ?> Dashboard</h3>

					   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

					   <div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link wp-element-button" href="#">View Dashboard</a></div>
			   </div>

			  </div>
			</div>
	</div>
