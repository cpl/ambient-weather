=== Ambient Weather ===
Contributors:      The CPL Team (Tyrone Fontaine and Will Skora)
Tags:              weather, cleveland
Tested up to:      6.4
Stable tag:        0.2.3
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Display a link to an Ambient Weather dashboard. The specific dashboards report weather conditions at different
CPL (Cleveland Public Library) locations and likely not useful to you unless you're in Cleveland, Ohio.

Further integration with the Ambient Weather API will be added.

Built with Create-Block(https://developer.wordpress.org/block-editor/reference-guides/packages/packages-create-block/) and uses wp-scripts

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/ambient-weather` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress

== For Development ==

1. Clone repository
2. run `npm run install` within the directory


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 0.2.3 =
* Additional santization and escaping of input from the Ambient Weather API
* stop duplicate, unecessary calls when converting degrees to cardinal direction.

= 0.2.2. =
* Improve keyboard interactions
* include the time when the data was last updated

= 0.2.0. =
* Add accessible modal with additional weather conditions for each location from the Ambient Weather API
* Cache the Ambient Weather API results

= 0.1.1 =
* include image assets from within the block itself

= 0.1.0 =
* Initial Release for cpl.org

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above. This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation." Arbitrary sections will be shown below the built-in sections outlined above.
